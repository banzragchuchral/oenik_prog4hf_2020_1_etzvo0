﻿// Enable XML documentation output
// <copyright file="Database.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Handles the read-write methods.
    /// </summary>
    public class Database : IDisposable
    {
        private static Database instance;

        private LibraryDatabaseEntities1 libraryDatabaseEntities;

        private Database()
        {
            this.libraryDatabaseEntities = new LibraryDatabaseEntities1();
        }

        /// <summary>
        /// Gets the instance of this class.
        /// </summary>
        public static Database Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Database();
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets all book.
        /// </summary>
        public List<Konyv> GetAllKonyv
        {
            get { return this.libraryDatabaseEntities.Konyv.Select(y => y).ToList(); }
        }

        /// <summary>
        /// Gets all book.
        /// </summary>
        public List<Konyvtar> GetAllKonyvtar
        {
            get { return this.libraryDatabaseEntities.Konyvtar.Select(y => y).ToList(); }
        }

        /// <summary>
        /// Gets all book.
        /// </summary>
        public List<Kolcsonzes> GetAllKolcsonzes
        {
            get { return this.libraryDatabaseEntities.Kolcsonzes.Select(y => y).ToList(); }
        }

        /// <summary>
        /// Gets all book.
        /// </summary>
        public List<Olvaso> GetAllOlvaso
        {
            get { return this.libraryDatabaseEntities.Olvaso.Select(y => y).ToList(); }
        }

        /// <summary>
        /// Gets the library instance.
        /// </summary>
        public virtual LibraryDatabaseEntities1 LibraryDatabase
        {
            get { return this.libraryDatabaseEntities; }
        }

        /// <summary>
        /// Add book.
        /// </summary>
        /// <param name="konyv">konyv.</param>
        public virtual void AddKonyv(Konyv konyv)
        {
            this.libraryDatabaseEntities.Konyv.Add(konyv);
        }

        /// <summary>
        /// Add library.
        /// </summary>
        /// <param name="konyvtar">konyvtar.</param>
        public virtual void AddKonyvtar(Konyvtar konyvtar)
        {
            this.libraryDatabaseEntities.Konyvtar.Add(konyvtar);
        }

        /// <summary>
        /// Add rent.
        /// </summary>
        /// <param name="kolcsonzes">kolcsonzes.</param>
        public virtual void AddKolcsonzes(Kolcsonzes kolcsonzes)
        {
            this.libraryDatabaseEntities.Kolcsonzes.Add(kolcsonzes);
        }

        /// <summary>
        /// Add reader.
        /// </summary>
        /// <param name="olvaso">olvaso.</param>
        public virtual void AddOlvaso(Olvaso olvaso)
        {
            this.libraryDatabaseEntities.Olvaso.Add(olvaso);
        }

        /// <summary>
        /// Save.
        /// </summary>
        public virtual void SaveDatabase()
        {
            this.libraryDatabaseEntities.SaveChanges();
        }

        /// <summary>
        /// Delete konyv.
        /// </summary>
        /// <param name="konyv">konyv.</param>
        public void DeleteKonyv(Konyv konyv)
        {
            if (this.libraryDatabaseEntities.Konyv.Any(x => x.Konyvid == konyv.Konyvid))
            {
                this.libraryDatabaseEntities.Konyv.Remove(konyv);
            }
        }

        /// <summary>
        /// Delete library.
        /// </summary>
        /// <param name="konyvtar">konyvtar.</param>
        public void DeleteKonyvtar(Konyvtar konyvtar)
        {
            if (this.libraryDatabaseEntities.Konyvtar.Any(x => x.Konyvtarid == konyvtar.Konyvtarid))
            {
                this.libraryDatabaseEntities.Konyvtar.Remove(konyvtar);
            }
        }

        /// <summary>
        /// Delete rent.
        /// </summary>
        /// <param name="kolcs">kolcs.</param>
        public void DeleteKolcsonzes(Kolcsonzes kolcs)
        {
            if (this.libraryDatabaseEntities.Kolcsonzes.Any(x => x.Kolcsonzesid == kolcs.Kolcsonzesid))
            {
                this.libraryDatabaseEntities.Kolcsonzes.Remove(kolcs);
            }
        }

        /// <summary>
        /// Delete reader.
        /// </summary>
        /// <param name="olvaso">olvaso.</param>
        public void DeleteOlvaso(Olvaso olvaso)
        {
            if (this.libraryDatabaseEntities.Olvaso.Any(x => x.Olvasoid == olvaso.Olvasoid))
            {
                this.libraryDatabaseEntities.Olvaso.Remove(olvaso);
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Auto generated.
        /// </summary>
        /// <param name="disposing">dispose.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (this.libraryDatabaseEntities != null)
                {
                    this.libraryDatabaseEntities.Dispose();
                }
            }
        }
    }
}
