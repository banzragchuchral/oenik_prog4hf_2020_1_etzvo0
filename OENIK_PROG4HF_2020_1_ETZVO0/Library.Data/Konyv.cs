//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Data
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Holds the book as a class.
    /// </summary>
    public partial class Konyv
    {
        /// <summary>
        /// Holds the id.
        /// </summary>
        public int Konyvid { get; set; }
        /// <summary>
        /// Holds the tite of the book.
        /// </summary>
        public string Cim { get; set; }
        /// <summary>
        /// Holds the author.
        /// </summary>
        public string Szerzo { get; set; }
        /// <summary>
        /// Holds the date.
        /// </summary>
        public System.DateTime Kiadas { get; set; }
        /// <summary>
        /// Holds the category.
        /// </summary>
        public string Konyvkateg { get; set; }
        /// <summary>
        /// Holds a foreign key.
        /// </summary>
        public int Konyvtarid { get; set; }
    }
}

