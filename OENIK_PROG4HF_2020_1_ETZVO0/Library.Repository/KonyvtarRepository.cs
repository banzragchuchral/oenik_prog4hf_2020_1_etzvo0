﻿// <copyright file="KonyvtarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Library.Data;

    /// <summary>
    /// Handles the library table repository.
    /// </summary>
    public class KonyvtarRepository : IRepository<Konyvtar>
    {
        /// <summary>
        /// Gets the books as queryable.
        /// </summary>
        public virtual IQueryable<Konyvtar> GetAll
        {
            get { return Database.Instance.GetAllKonyvtar.AsQueryable(); }
        }

        /// <summary>
        /// Gets returns a list of library data.
        /// </summary>
        /// <returns>liststring.</returns>
        public List<string> GetLibraries
        {
            get
            {
                List<Konyvtar> library = this.GetAll.ToList();
                List<string> vissza = new List<string>();

                foreach (var x in library)
                {
                    vissza.Add("ID: " + x.Konyvtarid + "| Név: " + x.Nev + "| Email: " + x.Email + "| Cím: " + x.Cim + "| Telefonszám: " + x.Telefonszam + "| Fax: " + x.Fax);
                }

                return vissza;
            }
        }

        /// <summary>
        /// Delete a library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public virtual bool Delete(int id)
        {
            List<Konyvtar> x = this.GetAll.Where(y => y.Konyvtarid == id).Select(y2 => y2).ToList();
            if (x.Count > 0)
            {
                Database.Instance.DeleteKonyvtar(x[0]);
                Database.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates the library table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">nev.</param>
        /// <param name="cim">cim.</param>
        /// <param name="email">email.</param>
        /// <param name="fax">fax.</param>
        /// <param name="telefonszam">telefonszam.</param>
        /// <returns>bool.</returns>
        public virtual bool Update(int id, string nev, string cim, string email, string fax, string telefonszam)
        {
            List<Konyvtar> x = this.GetAll.Where(y => y.Konyvtarid == id).Select(y2 => y2).ToList();
            if (x.Count > 0)
            {
                Konyvtar konyvtar = x[0];
                konyvtar.Cim = cim;
                konyvtar.Email = email;
                konyvtar.Fax = fax;
                konyvtar.Telefonszam = telefonszam;
                konyvtar.Nev = nev;
                Database.Instance.SaveDatabase();
            }

            return false;
        }

        /// <summary>
        /// Insert in to the Library table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">nev.</param>
        /// <param name="cim">cim.</param>
        /// <param name="email">email.</param>
        /// <param name="fax">fax.</param>
        /// <param name="telefonszam">telefonszam.</param>
        /// <returns>bool.</returns>
        public virtual bool Insert(int id, string nev, string cim, string email, string fax, string telefonszam)
        {
            Konyvtar konyvtar = new Konyvtar();
            konyvtar.Konyvtarid = id;
            konyvtar.Cim = cim;
            konyvtar.Email = email;
            konyvtar.Fax = fax;
            konyvtar.Telefonszam = telefonszam;
            konyvtar.Nev = nev;
            Database.Instance.AddKonyvtar(konyvtar);
            Database.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Checks if any Library exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public virtual bool CheckLibraryExist(int id)
        {
            return this.GetAll.Any(x => x.Konyvtarid == id);
        }

        /// <summary>
        /// Vissza adja, hogy a könyvtárnak hány könyve van.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>int.</returns>
        public virtual int Hanykonyv(int id)
        {
            KonyvRepository konyvrepo = new KonyvRepository();
            int x = this.GetAll.Single(y => y.Konyvtarid == id).Konyvtarid;

            // var z = from temp in konyvrepo.GetAll
            //        where temp.Konyvtarid == x
            //        group temp by temp.Konyvtarid into g
            //        select g.Count();
            var z = konyvrepo.GetAll.GroupBy(p => p.Konyvtarid == x)
                .Select(n => new
                {
                    nev = konyvrepo.GetAll.Select(p1 => p1.Konyvid),
                }).Count();

            return Convert.ToInt32(z);
        }
    }
}
