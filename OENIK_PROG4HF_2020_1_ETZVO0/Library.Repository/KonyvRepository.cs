﻿// <copyright file="KonyvRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Library.Data;

    /// <summary>
    /// Handles the books repository.
    /// </summary>
    public class KonyvRepository : IRepository<Konyv>
    {
        /// <summary>
        /// Gets a list of readers data.
        /// </summary>
        /// <returns>liststring.</returns>
        public List<string> GetBooks
        {
            get
            {
                List<Konyv> books = this.GetAll.ToList();
                List<string> vissza = new List<string>();

                foreach (var x in books)
                {
                    vissza.Add("ID: " + x.Konyvid + "| Cím: " + x.Cim + "| Szerzo: " + x.Szerzo + "| Kiadás: " + x.Kiadas + "| Konyvkateg: " + x.Konyvkateg + "| Konyvtarid: " + x.Konyvtarid);
                }

                return vissza;
            }
        }

        /// <summary>
        /// Gets the books as queryable.
        /// </summary>
        public virtual IQueryable<Konyv> GetAll
        {
            get { return Database.Instance.GetAllKonyv.AsQueryable(); }
        }

        /// <summary>
        /// Delete a book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool Delete(int id)
        {
            List<Konyv> x = this.GetAll.Where(y => y.Konyvid == id).Select(y2 => y2).ToList();
            if (x.Count > 0)
            {
                Database.Instance.DeleteKonyv(x[0]);
                Database.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update the table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="cim">cim.</param>
        /// <param name="kiadas">kiadas.</param>
        /// <param name="konyvkateg">konyvkateg.</param>
        /// <param name="szerzo">szerzo.</param>
        /// <param name="konyvtarid">konyvtarid.</param>
        /// <returns>bool.</returns>
        public virtual bool Update(int id, string cim, DateTime kiadas, string konyvkateg, string szerzo, int konyvtarid)
        {
            List<Konyv> x = this.GetAll.Where(y => y.Konyvid == id).Select(y2 => y2).ToList();
            if (x.Count > 0)
            {
                Konyv konyv = x[0];
                konyv.Cim = cim;
                konyv.Kiadas = kiadas;
                konyv.Konyvkateg = konyvkateg;
                konyv.Szerzo = szerzo;
                konyv.Konyvtarid = konyvtarid;
                Database.Instance.SaveDatabase();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Insert to the table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="cim">cim.</param>
        /// <param name="kiadas">kiadas.</param>
        /// <param name="konyvkateg">konyvkateg.</param>
        /// <param name="szerzo">szerzo.</param>
        /// <param name="konyvtarid">konyvtarid.</param>
        /// <returns>bool.</returns>
        public virtual bool Insert(int id, string cim, DateTime kiadas, string konyvkateg, string szerzo, int konyvtarid)
        {
            Konyv konyv = new Konyv();
            konyv.Konyvid = id;
            konyv.Cim = cim;
            konyv.Kiadas = kiadas;
            konyv.Konyvkateg = konyvkateg;
            konyv.Szerzo = szerzo;
            konyv.Konyvtarid = konyvtarid;
            Database.Instance.AddKonyv(konyv);
            Database.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Checks if any book exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool CheckBookExist(int id)
        {
            return this.GetAll.Any(x => x.Konyvid == id);
        }

        /// <summary>
        /// Returns book objects and the name of library.
        /// </summary>
        /// <param name="ev">ev.</param>
        /// <returns>IEnumerable.</returns>
        public virtual IEnumerable<object> Evalatt(int ev)
        {
            KonyvtarRepository konyvtar = new KonyvtarRepository();
            var megfelelokonyvek = from x in this.GetAll
                                            join y in konyvtar.GetAll on x.Konyvtarid equals y.Konyvtarid
                                            where x.Kiadas.Year < ev
                                            select new { konyvneve = x.Cim, konyvtarneve = y.Nev };
            return megfelelokonyvek;
        }
    }
}
