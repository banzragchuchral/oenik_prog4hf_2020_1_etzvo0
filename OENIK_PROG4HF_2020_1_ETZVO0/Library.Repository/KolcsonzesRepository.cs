﻿// <copyright file="KolcsonzesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Library.Data;

    /// <summary>
    /// Holds the rents repository.
    /// </summary>
    public class KolcsonzesRepository : IRepository<Kolcsonzes>
    {
        /// <summary>
        /// Gets the books as queryable.
        /// </summary>
        public virtual IQueryable<Kolcsonzes> GetAll
        {
            get { return Database.Instance.GetAllKolcsonzes.AsQueryable(); }
        }

        /// <summary>
        /// Gets a list of rents data.
        /// </summary>
        /// <returns>liststring.</returns>
        public List<string> GetRents
        {
            get
            {
                List<Kolcsonzes> rents = this.GetAll.ToList();
                List<string> vissza = new List<string>();

                foreach (var x in rents)
                {
                    vissza.Add("ID: " + x.Kolcsonzesid + "| Konyvid: " + x.Konyvid + "| Olvasoid: " + x.Olvasoid + "| Datum: " + x.Datum);
                }

                return vissza;
            }
        }

        /// <summary>
        /// Delete a rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public virtual bool Delete(int id)
        {
            List<Kolcsonzes> x = this.GetAll.Where(y => y.Kolcsonzesid == id).Select(y2 => y2).ToList();
            if (x.Count() > 0)
            {
                Database.Instance.DeleteKolcsonzes(x[0]);
                Database.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update the rent table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="konyvid">konyvid.</param>
        /// <param name="olvasoid">olvasoid.</param>
        /// <param name="date">date.</param>
        /// <returns>bool.</returns>
        public bool Update(int id, int konyvid, int olvasoid, DateTime date)
        {
            List<Kolcsonzes> x = this.GetAll.Where(y => y.Kolcsonzesid == id).Select(y2 => y2).ToList();
            if (x.Count > 0)
            {
                Kolcsonzes kolcs = x[0];
                kolcs.Konyvid = konyvid;
                kolcs.Olvasoid = olvasoid;
                kolcs.Datum = date;
                Database.Instance.SaveDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Insert to the table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="konyvid">konyvid.</param>
        /// <param name="olvasoid">olvasoid.</param>
        /// <param name="datum">datum.</param>
        /// <returns>bool.</returns>
        public virtual bool Insert(int id, int konyvid, int olvasoid, DateTime datum)
        {
            Kolcsonzes kolcs = new Kolcsonzes();
            kolcs.Kolcsonzesid = id;
            kolcs.Konyvid = konyvid;
            kolcs.Olvasoid = olvasoid;
            kolcs.Datum = datum;
            Database.Instance.AddKolcsonzes(kolcs);
            Database.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Checks if any rent exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public virtual bool CheckRentExist(int id)
        {
            return this.GetAll.Any(x => x.Kolcsonzesid == id);
        }

        /// <summary>
        /// Return how many rents are in the dabase.
        /// </summary>
        /// <returns>int.</returns>
        public virtual int HanyKolcsonzes()
        {
            int a = this.GetAll.Count();
            return a;
        }
    }
}
