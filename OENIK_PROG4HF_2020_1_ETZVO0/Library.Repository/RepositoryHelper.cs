﻿// <copyright file="RepositoryHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Repository
{
    using System.Collections.Generic;
    using Library.Data;

    /// <summary>
    /// This class will get repositories for logic class.
    /// </summary>
    public class RepositoryHelper
    {
        private readonly KonyvRepository konyvrep;
        private readonly KonyvtarRepository konyvtarrep;
        private readonly KolcsonzesRepository kolcsonzesrep;
        private readonly OlvasoRepository olvasorep;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryHelper"/> class.
        /// Constructor creates new repositories.
        /// </summary>
        public RepositoryHelper()
        {
            this.konyvrep = new KonyvRepository();
            this.konyvtarrep = new KonyvtarRepository();
            this.kolcsonzesrep = new KolcsonzesRepository();
            this.olvasorep = new OlvasoRepository();
        }

        public RepositoryHelper(LibraryDatabaseEntities1 ctx)
        {
            this.konyvrep = new KonyvRepository();
            this.konyvtarrep = new KonyvtarRepository();
            this.kolcsonzesrep = new KolcsonzesRepository();
            this.olvasorep = new OlvasoRepository(ctx);
        }

        /// <summary>
        /// Gets the book repository.
        /// </summary>
        public virtual KonyvRepository KonyvRepository
        {
            get { return this.konyvrep; }
        }

        /// <summary>
        /// Gets the library repository.
        /// </summary>
        public virtual KonyvtarRepository KonyvtarRepository
        {
            get { return this.konyvtarrep; }
        }

        /// <summary>
        /// Gets the rent repository.
        /// </summary>
        public virtual KolcsonzesRepository KolcsonzesRepository
        {
            get { return this.kolcsonzesrep; }
        }

        /// <summary>
        /// Gets the reader repository.
        /// </summary>
        public virtual OlvasoRepository OlvasoRepository
        {
            get { return this.olvasorep; }
        }
    }
}
