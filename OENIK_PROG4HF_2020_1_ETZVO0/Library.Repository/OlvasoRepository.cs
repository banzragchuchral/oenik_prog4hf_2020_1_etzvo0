﻿// <copyright file="OlvasoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Library.Data;

    /// <summary>
    /// Handles the readers repository.
    /// </summary>
    public class OlvasoRepository : IRepository<Olvaso>
    {
        LibraryDatabaseEntities1 database;

        public OlvasoRepository(LibraryDatabaseEntities1 ctx)
        {
            database = ctx;
        }


        public OlvasoRepository()
        {

        }
        /// <summary>
        /// Gets the books as queryable.
        /// </summary>
        public virtual IQueryable<Olvaso> GetAll
        {
            get { return Database.Instance.GetAllOlvaso.AsQueryable(); }
        }

        /// <summary>
        /// Gets returns a list of readers data.
        /// </summary>
        /// <returns>liststring.</returns>
        public List<string> GetReaders
        {
            get
            {
                List<Olvaso> olvasok = this.GetAll.ToList();
                List<string> vissza = new List<string>();

                foreach (var x in olvasok)
                {
                    vissza.Add("ID: " + x.Olvasoid + "| Név: " + x.Nev + "| Szül. dát.: " + x.Szuletesidatum.ToString() + "| Email: " + x.Email + "| Cím: " + x.Cim + "| Telefonszám: " + x.Telefonszam);
                }

                return vissza;
            }
        }

        /// <summary>
        /// Delete a reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public virtual bool Delete(int id)
        {
            List<Olvaso> x = this.GetAll.Where(y => y.Olvasoid == id).Select(y2 => y2).ToList();
            if (x.Count() > 0)
            {
                Database.Instance.DeleteOlvaso(x[0]);
                Database.Instance.SaveDatabase();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Update the reader table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">nev.</param>
        /// <param name="cim">cim.</param>
        /// <param name="szul">szul.</param>
        /// <param name="email">email.</param>
        /// <param name="telefonszam">telefonszam.</param>
        /// <returns>bool.</returns>
        public virtual bool Update(int id, string nev, string cim, DateTime szul, string email, string telefonszam)
        {
            List<Olvaso> x = this.GetAll.Where(y => y.Olvasoid == id).Select(y2 => y2).ToList();
            if (x.Count > 0)
            {
                Olvaso olvaso = x[0];
                olvaso.Szuletesidatum = szul;
                olvaso.Cim = cim;
                olvaso.Email = email;
                olvaso.Telefonszam = telefonszam;
                olvaso.Nev = nev;
                Database.Instance.SaveDatabase();
            }

            return false;
        }

        /// <summary>
        /// Insert into the reader table.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">nev.</param>
        /// <param name="cim">cim.</param>
        /// <param name="email">email.</param>
        /// <param name="telefonszam">telefonszam.</param>
        /// <param name="szul">szul.</param>
        /// <returns>bool.</returns>
        public virtual bool Insert(int id, string nev, string cim, string email, string telefonszam, DateTime szul)
        {
            Olvaso olvaso = new Olvaso();
            olvaso.Olvasoid = id;
            olvaso.Cim = cim;
            olvaso.Email = email;
            olvaso.Telefonszam = telefonszam;
            olvaso.Nev = nev;
            olvaso.Szuletesidatum = szul;
            Database.Instance.AddOlvaso(olvaso);
            Database.Instance.SaveDatabase();
            return true;
        }

        /// <summary>
        /// Checks if any reader exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public virtual bool CheckReaderExist(int id)
        {
            return this.GetAll.Any(x => x.Olvasoid == id);
        }

        /// <summary>
        /// Returns the number of phone number according to the received number.
        /// </summary>
        /// <param name="szam">szam.</param>
        /// <returns>int.</returns>
        public virtual IEnumerable<object> Hanymobilszam(string szam)
        {
            KolcsonzesRepository kolcsonzes = new KolcsonzesRepository();
            var a = from x in this.GetAll
                    join y in kolcsonzes.GetAll on x.Olvasoid equals y.Olvasoid
                    where x.Telefonszam.Substring(2, 2) == szam
                    select new { olvasoneve = x.Nev, telefonszam = x.Telefonszam, konyvid = y.Konyvid };

            return a;
        }

        public Olvaso GetOne(int id)
        {
            return GetAll.SingleOrDefault(x => x.Olvasoid == id);
        }

        public IQueryable<Olvaso> GetAll2()
        {
            return GetAll;
        }

        public bool Remove(int id)
        {
            Olvaso entity = GetOne(id);
            if (entity == null) return false;
            database.Set<Olvaso>().Remove(entity);
            database.SaveChanges();
            return true;
        }
    }
}
