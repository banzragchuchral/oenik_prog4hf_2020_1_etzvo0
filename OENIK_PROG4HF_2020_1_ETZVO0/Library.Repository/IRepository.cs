﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Repository
{
    using System.Linq;

    /// <summary>
    /// Interface for Repository.
    /// </summary>
    /// <typeparam name="T">T type.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets all T type.
        /// </summary>
        IQueryable<T> GetAll { get; }

        /// <summary>
        /// Delete an element.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool Delete(int id);
    }
}
