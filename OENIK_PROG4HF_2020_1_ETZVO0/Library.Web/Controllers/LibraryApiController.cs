﻿using AutoMapper;
using Library.Data;
using Library.Logic;
using Library.Repository;
using Library.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Library.Web.Controllers
{
    public class LibraryApiController : ApiController
    {
        public class ApiResult
        {
            public bool Operationresult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public LibraryApiController()
        {
            LibraryDatabaseEntities1 ctx = new LibraryDatabaseEntities1();
            RepositoryHelper rep = new RepositoryHelper(ctx);
            logic = new LogicClass(rep);
            mapper = MapperFactory.CreateMapper();

        }

        //GET api/LibraryApi
        [ActionName("all")]
        [HttpGet]
        //GET api/LibraryApi/all
        public IEnumerable<Models.Olvaso> GetAll()
        {
            var readers = logic.GetOlvaso2();
            return mapper.Map<IList<Data.Olvaso>, List<Models.Olvaso>>(readers);
        }

        //Get api/LibraryApi/del/42
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneReader(int id)
        {
            bool succes = logic.RemoveReader(id);
            return new ApiResult() { Operationresult = succes };
        }
        //Get api/LibraryApi/add + car
        [ActionName("add")]
        [HttpPost]
        public ApiResult DelOneReader(Models.Olvaso olvaso)
        {
            logic.InsertOlvaso(olvaso.Olvasoid, olvaso.Nev, olvaso.Cim, olvaso.Email, olvaso.Telefonszam, olvaso.Szuletesidatum);
            return new ApiResult() { Operationresult = true };
        }
        //Get api/LibraryApi/mod + car
        [ActionName("mode")]
        [HttpPost]
        public ApiResult ModOneReader(Models.Olvaso olvaso)
        {
            bool success = logic.UpdateOlvaso(olvaso.Olvasoid, olvaso.Nev, olvaso.Cim, olvaso.Email, olvaso.Telefonszam, olvaso.Szuletesidatum);
            return new ApiResult() { Operationresult = success };
        }
    }
}
