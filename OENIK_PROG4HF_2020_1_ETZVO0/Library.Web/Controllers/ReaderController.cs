﻿using AutoMapper;
using Library.Data;
using Library.Logic;
using Library.Repository;
using Library.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Web.Controllers
{
    public class ReaderController : Controller
    {

        ILogic logic;
        OlvasoViewModel vm;
        IMapper mapper; 

        public ReaderController()
        {
            LibraryDatabaseEntities1 ctx = new LibraryDatabaseEntities1();
            RepositoryHelper rep = new RepositoryHelper(ctx);
            logic = new LogicClass(rep);
            mapper = MapperFactory.CreateMapper();

            vm = new OlvasoViewModel();
            vm.EditedReader = new Models.Olvaso();
            var readers = logic.GetOlvaso2();
            vm.ListofReaders = mapper.Map<IList<Data.Olvaso>, List<Models.Olvaso>>(readers);

        }

        private Models.Olvaso GetReaderModel(int id)
        {
            Library.Data.Olvaso olvaso = logic.GetOlvaso2(id);
            return mapper.Map<Data.Olvaso, Models.Olvaso>(olvaso);
        }

        // GET: Reader
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("ReaderIndex", vm);
        }

        // GET: Reader/Details/5
        public ActionResult Details(int id)
        {
            return View("ReaderDetails", GetReaderModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.DeleteOlvaso(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }

        //GET: /Reader/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedReader = GetReaderModel(id);
            return View("ReaderIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Models.Olvaso reader, string editAction)
        {
            if (ModelState.IsValid && reader != null)
            {
                TempData["editResult"] = "Edit OK";
                if(editAction == "AddNew")
                {
                    bool success = logic.InsertOlvaso(reader.Olvasoid, reader.Nev, reader.Cim, reader.Email, reader.Telefonszam, reader.Szuletesidatum);
                }
                else
                {
                    if (!logic.UpdateOlvaso(reader.Olvasoid, reader.Nev, reader.Cim, reader.Email, reader.Telefonszam, reader.Szuletesidatum))
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedReader = reader;
                return View("ReaderIndex", vm);
            }
        }
    }
}
