﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Library.Data.Olvaso, Library.Web.Models.Olvaso>().
                    ForMember(dest => dest.Olvasoid, map => map.MapFrom(src => src.Olvasoid)).
                    ForMember(dest => dest.Nev, map => map.MapFrom(src => src.Nev)).
                    ForMember(dest => dest.Cim, map => map.MapFrom(src => src.Cim)).
                    ForMember(dest => dest.Telefonszam, map => map.MapFrom(src => src.Telefonszam)).
                    ForMember(dest => dest.Email, map => map.MapFrom(src => src.Email)).
                    ForMember(dest => dest.Szuletesidatum, map => map.MapFrom(src => src.Szuletesidatum));
            });

            return config.CreateMapper();
        }
    }
}