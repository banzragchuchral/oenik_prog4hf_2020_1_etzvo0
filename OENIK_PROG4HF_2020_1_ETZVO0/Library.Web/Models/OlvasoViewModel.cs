﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Web.Models
{
    public class OlvasoViewModel
    {
        public Olvaso EditedReader { get; set; }
        public List<Olvaso> ListofReaders { get; set; }
    }
}