﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Web.Models
{
    public class Olvaso
    {
        /// <summary>
        /// Holds the id.
        /// </summary>
        [Display(Name = "ID")]
        [Required]
        public int Olvasoid { get; set; }
        /// <summary>
        /// Holds the name.
        /// </summary>
        [Display(Name = "Name")]
        [Required]
        public string Nev { get; set; }
        /// <summary>
        /// Holds the birth date.
        /// </summary>
        [Display(Name = "Birthdate")]
        [Required]
        public DateTime Szuletesidatum { get; set; }
        /// <summary>
        /// Holds the address.
        /// </summary>
        [Display(Name = "Address")]
        [Required]
        public string Cim { get; set; }
        /// <summary>
        /// Holds the email.
        /// </summary>
        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// Holds the phone number.
        /// </summary>
        [Display(Name = "Phone number")]
        [Required]
        public string Telefonszam { get; set; }
    }
}