﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Program
{
    using System;
    using System.Net;
    using System.Text;
    using System.Xml.Linq;
    using Library.Logic;

    /// <summary>
    /// Holds the menu.
    /// </summary>
    public static class Menu
    {
        /// <summary>
        /// Menu start here.
        /// </summary>
        public static void MenuStatic()
        {
            Logic.LogicClass logic = new LogicClass();
            bool kilepes = false;

            while (!kilepes)
            {
                DefaultMenu();
                bool c;
                int bemenetiszam;
                string bemenet;

                do
                {
                    Console.WriteLine("Válaszd ki a megfelelő számot!");
                    bemenet = Console.ReadLine();
                    c = int.TryParse(bemenet, out bemenetiszam);
                }
                while (!c);

                bool kilepes_belso = false;

                if (c)
                {
                    switch (bemenetiszam)
                    {
                        case 1:
                            while (!kilepes_belso)
                            {
                                Console.Clear();
                                BelsoMenu("Kölcsönzések");
                                do
                                {
                                    Console.WriteLine("Válaszd ki a megfelelő számot!");
                                    bemenet = Console.ReadLine();
                                    c = int.TryParse(bemenet, out bemenetiszam);
                                }
                                while (!c);

                                int id, konyvid, olvasoid;
                                string date2;
                                DateTime date;
                                switch (bemenetiszam)
                                {
                                    case 1:
                                        foreach (var item in logic.GetKolcsonzes)
                                        {
                                            Console.WriteLine(item);
                                        }

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 2:

                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        while (logic.CheckifExistKolcsonzes(id))
                                        {
                                            Console.WriteLine("Ilyen id már van válassz másikat!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy konyvid-t: ");
                                        konyvid = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyv(konyvid))
                                        {
                                            Console.WriteLine("Ilyen konyvid nem létezik válassz másikat!");
                                            konyvid = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy olvasoid-t: ");
                                        olvasoid = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyv(olvasoid))
                                        {
                                            Console.WriteLine("Ilyen olvasoid nem létezik válassz másikat!");
                                            olvasoid = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy dátumot: (YYYY.MM.DD)");
                                        date2 = Console.ReadLine();

                                        while (!DateTime.TryParse(date2, out date))
                                        {
                                            Console.WriteLine("Nem jól megadott dátum (YYYY.MM.DD)");
                                            date2 = Console.ReadLine();
                                        }

                                        logic.InsertKolcsonzes(id, konyvid, olvasoid, date);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 3:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKolcsonzes(id))
                                        {
                                            Console.WriteLine("Nem létezik ilyen id!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy konyvid-t: ");
                                        konyvid = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyv(konyvid))
                                        {
                                            Console.WriteLine("Ilyen konyvid nem létezik válassz másikat!");
                                            konyvid = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy olvasoid-t: ");
                                        olvasoid = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyv(olvasoid))
                                        {
                                            Console.WriteLine("Ilyen olvasoid nem létezik válassz másikat!");
                                            olvasoid = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy dátumot: (YYYY.MM.DD)");
                                        date2 = Console.ReadLine();

                                        while (!DateTime.TryParse(date2, out date))
                                        {
                                            Console.WriteLine("Nem jól megadott dátum (YYYY.MM.DD)");
                                            date2 = Console.ReadLine();
                                        }

                                        logic.UpdateKolcsonzes(id, konyvid, olvasoid, date);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 4:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKolcsonzes(id))
                                        {
                                            Console.WriteLine("Nem létezik ilyen id!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        logic.DeleteKolcsonzes(id);
                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 0:
                                        kilepes_belso = true;
                                        break;
                                    default:
                                        Console.WriteLine("Nincs ilyen");
                                        break;
                                }
                            }

                            break;
                        case 2:
                            while (!kilepes_belso)
                            {
                                Console.Clear();
                                BelsoMenu("Olvasók");
                                do
                                {
                                    Console.WriteLine("Válaszd ki a megfelelő számot!");
                                    bemenet = Console.ReadLine();
                                    c = int.TryParse(bemenet, out bemenetiszam);
                                }
                                while (!c);

                                int id;
                                string nev, cim, email, telefonszam, date2;
                                DateTime date;
                                switch (bemenetiszam)
                                {
                                    case 1:
                                        foreach (var item in logic.GetOlvaso)
                                        {
                                            Console.WriteLine(item);
                                        }

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 2:

                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (logic.CheckifExistOlvaso(id))
                                        {
                                            Console.WriteLine("Ilyen id már van válassz másikat!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy nevet: ");
                                        nev = Console.ReadLine();

                                        Console.WriteLine("Kérek egy cimet: ");
                                        cim = Console.ReadLine();

                                        Console.WriteLine("Kérek egy emailt: ");
                                        email = Console.ReadLine();

                                        Console.WriteLine("Kérek egy telefonszamot: ");
                                        telefonszam = Console.ReadLine();

                                        Console.WriteLine("Kérek egy szul. datumot: ");
                                        date2 = Console.ReadLine();

                                        while (!DateTime.TryParse(date2, out date))
                                        {
                                            Console.WriteLine("Nem jól megadott dátum (YYYY.MM.DD)");
                                            date2 = Console.ReadLine();
                                        }

                                        logic.InsertOlvaso(id, nev, cim, email, telefonszam, date);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 3:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistOlvaso(id))
                                        {
                                            Console.WriteLine("Nem létezik ilyen id!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy nevet: ");
                                        nev = Console.ReadLine();
                                        Console.WriteLine("Kérek egy cimet: ");
                                        cim = Console.ReadLine();
                                        Console.WriteLine("Kérek egy emailt: ");
                                        email = Console.ReadLine();
                                        Console.WriteLine("Kérek egy telefonszamot: ");
                                        telefonszam = Console.ReadLine();
                                        Console.WriteLine("Kérek egy szul. datumot: ");
                                        date2 = Console.ReadLine();

                                        while (!DateTime.TryParse(date2, out date))
                                        {
                                            Console.WriteLine("Nem jól megadott dátum (YYYY.MM.DD)");
                                            date2 = Console.ReadLine();
                                        }

                                        logic.UpdateOlvaso(id, nev, cim, email, telefonszam, date);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 4:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistOlvaso(id))
                                        {
                                            Console.WriteLine("Nem létezik ilyen id!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        logic.DeleteOlvaso(id);
                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 0:
                                        kilepes_belso = true;
                                        break;
                                    default:
                                        Console.WriteLine("Nincs ilyen");
                                        break;
                                }
                            }

                            break;
                        case 3:
                            while (!kilepes_belso)
                            {
                                Console.Clear();
                                BelsoMenu("Könyvek");
                                do
                                {
                                    Console.WriteLine("Válaszd ki a megfelelő számot!");
                                    bemenet = Console.ReadLine();
                                    c = int.TryParse(bemenet, out bemenetiszam);
                                }
                                while (!c);

                                int id, konyvtarid;
                                string cim, categ, szerzo, date2;
                                DateTime date;
                                switch (bemenetiszam)
                                {
                                    case 1:
                                        foreach (var item in logic.GetKonyv)
                                        {
                                            Console.WriteLine(item);
                                        }

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 2:

                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (logic.CheckifExistKonyv(id))
                                        {
                                            Console.WriteLine("Ilyen id már van válassz másikat!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy cimet: ");
                                        cim = Console.ReadLine();
                                        Console.WriteLine("Kérek egy szerzo: ");
                                        szerzo = Console.ReadLine();
                                        Console.WriteLine("Kérek egy kategoriat: ");
                                        categ = Console.ReadLine();
                                        Console.WriteLine("Kérek egy kiadasi datumot: ");
                                        date2 = Console.ReadLine();

                                        while (!DateTime.TryParse(date2, out date))
                                        {
                                            Console.WriteLine("Nem jól megadott dátum (YYYY.MM.DD)");
                                            date2 = Console.ReadLine();
                                        }

                                        Console.WriteLine("Kérek egy könyvtárd id-t");
                                        konyvtarid = Convert.ToInt32(Console.ReadLine());
                                        while (!logic.CheckifExistKonyvtar(konyvtarid))
                                        {
                                            Console.WriteLine("Nincs ilyen könyvtár! Add meg újra");
                                            konyvtarid = Convert.ToInt32(Console.ReadLine());
                                        }

                                        logic.InsertKonyv(id, cim, date, categ, szerzo, konyvtarid);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 3:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyv(id))
                                        {
                                            Console.WriteLine("Nincs ilyen id!");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy cimet: ");
                                        cim = Console.ReadLine();
                                        Console.WriteLine("Kérek egy szerzo: ");
                                        szerzo = Console.ReadLine();
                                        Console.WriteLine("Kérek egy kategoriat: ");
                                        categ = Console.ReadLine();
                                        Console.WriteLine("Kérek egy kiadasi datumot: ");
                                        date2 = Console.ReadLine();

                                        while (!DateTime.TryParse(date2, out date))
                                        {
                                            Console.WriteLine("Nem jól megadott dátum (YYYY.MM.DD)");
                                            date2 = Console.ReadLine();
                                        }

                                        Console.WriteLine("Kérek egy könyvtárd id-t");
                                        konyvtarid = Convert.ToInt32(Console.ReadLine());
                                        while (!logic.CheckifExistKonyvtar(konyvtarid))
                                        {
                                            Console.WriteLine("Nincs ilyen könyvtár! Add meg újra");
                                            konyvtarid = Convert.ToInt32(Console.ReadLine());
                                        }

                                        logic.UpdateKonyv(id, cim, date, categ, szerzo, konyvtarid);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 4:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());
                                        while (!logic.CheckifExistKonyv(id))
                                        {
                                            Console.WriteLine("Nincs ilyen könyv! Add meg újra");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        logic.DeleteKonyv(id);
                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 0:
                                        kilepes_belso = true;
                                        break;
                                    default:
                                        Console.WriteLine("Nincs ilyen");
                                        break;
                                }
                            }

                            break;
                        case 4:
                            while (!kilepes_belso)
                            {
                                Console.Clear();
                                BelsoMenu("Könyvtárak");
                                do
                                {
                                    Console.WriteLine("Válaszd ki a megfelelő számot!");
                                    bemenet = Console.ReadLine();
                                    c = int.TryParse(bemenet, out bemenetiszam);
                                }
                                while (!c);

                                int id;
                                string cim, nev, email, telefon, fax;
                                switch (bemenetiszam)
                                {
                                    case 1:
                                        foreach (var item in logic.GetKonyvtar)
                                        {
                                            Console.WriteLine(item);
                                        }

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 2:

                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (logic.CheckifExistKonyvtar(id))
                                        {
                                            Console.WriteLine("Van már ilyen könyvtár! Add meg újra");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy nevet: ");
                                        nev = Console.ReadLine();

                                        Console.WriteLine("Kérek egy cimet: ");
                                        cim = Console.ReadLine();

                                        Console.WriteLine("Kérek egy emailt: ");
                                        email = Console.ReadLine();

                                        Console.WriteLine("Kérek egy telefonszamot: ");
                                        telefon = Console.ReadLine();

                                        Console.WriteLine("Kérek egy faxot: ");
                                        fax = Console.ReadLine();

                                        logic.InsertKonyvtar(id, nev, cim, email, fax, telefon);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 3:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyvtar(id))
                                        {
                                            Console.WriteLine("Nincs ilyen könyvtár! Add meg újra");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        Console.WriteLine("Kérek egy nevet: ");
                                        nev = Console.ReadLine();

                                        Console.WriteLine("Kérek egy cimet: ");
                                        cim = Console.ReadLine();

                                        Console.WriteLine("Kérek egy emailt: ");
                                        email = Console.ReadLine();

                                        Console.WriteLine("Kérek egy telefonszamot: ");
                                        telefon = Console.ReadLine();

                                        Console.WriteLine("Kérek egy faxot: ");
                                        fax = Console.ReadLine();

                                        logic.UpdateKonyvtar(id, nev, cim, email, fax, telefon);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 4:
                                        Console.WriteLine("Kérek egy id-t: ");
                                        id = Convert.ToInt32(Console.ReadLine());

                                        while (!logic.CheckifExistKonyvtar(id))
                                        {
                                            Console.WriteLine("Nincs ilyen könyv! Add meg újra");
                                            id = Convert.ToInt32(Console.ReadLine());
                                        }

                                        logic.DeleteKonyvtar(id);

                                        Console.WriteLine("Nyomj egy gombot a folytatáshoz.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    case 0:
                                        kilepes_belso = true;
                                        break;
                                    default:
                                        Console.WriteLine("Nincs ilyen");
                                        break;
                                }
                            }

                            break;
                        case 5:
                            Console.Clear();
                            Console.WriteLine("Adj egy id-t!");
                            int valami = Convert.ToInt32(Console.ReadLine());
                            while (!logic.CheckifExistKonyvtar(valami))
                            {
                                Console.WriteLine("Nincs ilyen könyvtár! Add meg újra");
                                valami = Convert.ToInt32(Console.ReadLine());
                            }

                            Console.WriteLine(logic.HanyKonyv(valami));

                            Console.WriteLine("Nyomj valamit a folytatáshoz.");
                            Console.ReadKey();
                            break;
                        case 6:
                            Console.Clear();
                            Console.WriteLine("Adj meg egy mobil szolgáltatott! (20,30,70)");
                            string mobilszolgaltato;
                            do
                            {
                                mobilszolgaltato = Console.ReadLine();
                            }
                            while (mobilszolgaltato != "20" && mobilszolgaltato != "30" && mobilszolgaltato != "70");
                            var olvasok = logic.HanyMobil(mobilszolgaltato);
                            foreach (var olvaso in olvasok)
                            {
                                Console.WriteLine(olvaso);
                            }

                            Console.WriteLine("Nyomj valamit a folytatáshoz.");
                            Console.ReadKey();
                            break;
                        case 7:
                            Console.Clear();
                            Console.WriteLine("Adj egy évet!");
                            string helyettes = Console.ReadLine();

                            while (!int.TryParse(helyettes, out valami))
                            {
                                Console.WriteLine("Adj meg egy megfelelőt!");
                                valami = Convert.ToInt32(Console.ReadLine());
                            }

                            var x = logic.Evalatt(valami);
                            foreach (var x1 in x)
                            {
                                Console.WriteLine(x1.ToString());
                            }

                            Console.WriteLine("Nyomj valamit a folytatáshoz.");
                            Console.ReadKey();
                            break;
                        case 8:
                            Console.Clear();
                            using (WebClient client = new WebClient())
                            {
                                string javaurl = "http://localhost:8080/PROG3_2019_2_ETZVO0/?eletkor=";

                                Console.WriteLine("Add meg a korod");
                                string kor = Console.ReadLine();

                                client.Encoding = Encoding.UTF8;
                                string valassz = client.DownloadString(javaurl + kor);

                                Console.WriteLine("Neked ajánlott könyv címe: ");
                                Console.WriteLine(valassz);
                            }

                            Console.WriteLine("Nyomj valamit a folytatáshoz.");
                            Console.ReadKey();
                            break;
                        case 0:
                            kilepes = true;
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Hibás! Írja be újra!");
                    c = int.TryParse(bemenet, out bemenetiszam);
                }
            }
        }

        private static void DefaultMenu()
        {
            System.Console.WriteLine("1: Kölcsönzések listázása / hozzáadása / módosítása / törlése");
            System.Console.WriteLine("2: Olvasók listázása / hozzáadása / módosítása / törlése");
            System.Console.WriteLine("3: Könyv listázása / hozzáadása / módosítása / törlése");
            System.Console.WriteLine("4: Könyvtár listázása / hozzáadása / módosítása / törlése");
            System.Console.WriteLine("5: Hány ilyen könyv van ezzel a könyvtárid-val");
            System.Console.WriteLine("6: Olvasók, akinek ilyen mobilszolgáltatójuk van és a könyvek amiket ki kölcsönöztek.");
            System.Console.WriteLine("7: Könyvek, amik régebbiek mint az adott év és a könyvtár ahová tartoznak");
            System.Console.WriteLine("8: Java Web");
            System.Console.WriteLine("0: Kilépés");
        }

        private static void BelsoMenu(string a)
        {
            Console.WriteLine("1: " + a + " listázása.");
            Console.WriteLine("2: " + a + " hozzáadása.");
            Console.WriteLine("3: " + a + " módosítása.");
            Console.WriteLine("4: " + a + " törlése.");
            Console.WriteLine("0: Kilépés");
        }
    }
}
