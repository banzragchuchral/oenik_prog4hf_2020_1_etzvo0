﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Program
{
    /// <summary>
    /// Program starts here.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// This is where the program launches.
        /// </summary>
        public static void Main()
        {
            Menu.MenuStatic();
        }
    }
}
