﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Library.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:52553/api/LibraryApi/";

        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ReaderResult");
        }

        public List<LibraryVm> ApiGetReaders()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<LibraryVm>>(json);
            // SendMessage(true);
            return list;
        }

        public void ApiDelReader(LibraryVm reader)
        {
            bool success = false;
            if(reader!=null)
            {
                string json = client.GetStringAsync(url + "del/" + reader.Olvasoid).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditReader(LibraryVm reader, bool isEditing)
        {
            if (reader == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add(nameof(LibraryVm.Olvasoid), reader.Olvasoid.ToString());
            postData.Add(nameof(LibraryVm.Nev), reader.Nev);
            postData.Add(nameof(LibraryVm.Email), reader.Email);
            postData.Add(nameof(LibraryVm.Cim), reader.Cim);
            postData.Add(nameof(LibraryVm.Telefonszam), reader.Telefonszam);
            postData.Add(nameof(LibraryVm.Szuletesidatum), reader.Szuletesidatum.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditCar(LibraryVm reader, Func<LibraryVm, bool> editor)
        {
            LibraryVm clone = new LibraryVm();
            if (reader != null) clone.CopyFrom(reader);
            bool? success = editor?.Invoke(clone);
            if(success == true)
            {
                if (reader != null) ApiEditReader(clone, true);
                else ApiEditReader(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
