﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Library.Wpf
{
    class MainVm : ViewModelBase
    {
        private MainLogic logic;
        private LibraryVm selectedreader;
        private ObservableCollection<LibraryVm> allreaders;

        public ObservableCollection<LibraryVm> AllReaders
        {
            get { return allreaders; }
            set { Set(ref allreaders, value); }
        }

        public LibraryVm Seletedreader
        {
            get { return selectedreader; }
            set { Set(ref selectedreader, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<LibraryVm, bool> EditorFunc { get; set; }

        public MainVm()
        {
            logic = new MainLogic();

            DelCmd = new RelayCommand(() => logic.ApiDelReader(selectedreader));
            AddCmd = new RelayCommand(() => logic.EditCar(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditCar(Seletedreader, EditorFunc));
            LoadCmd = new RelayCommand(() => AllReaders = new ObservableCollection<LibraryVm>(logic.ApiGetReaders()));

        }
    }
}
