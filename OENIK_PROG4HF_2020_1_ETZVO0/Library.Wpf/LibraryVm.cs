﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Wpf
{
    class LibraryVm : ObservableObject
    {
        private int olvasoid;
        private string nev;
        private string cim;
        private string email;
        private string telefonszam;
        private DateTime szuletesidatum;

        public DateTime Szuletesidatum
        {
            get { return szuletesidatum; }
            set { Set(ref szuletesidatum, value); }
        }


        public string Telefonszam
        {
            get { return telefonszam; }
            set { Set(ref telefonszam, value); }
        }


        public string Email
        {
            get { return email; }
            set { Set(ref email, value); }
        }

        public string Cim
        {
            get { return cim; }
            set { Set(ref cim, value); }
        }


        public string Nev
        {
            get { return nev; }
            set { Set(ref nev, value); }
        }

        public int Olvasoid
        {
            get { return olvasoid; }
            set { Set(ref olvasoid, value); }
        }

        public void CopyFrom(LibraryVm other)
        {
            if (other == null) return;
            this.Olvasoid = other.Olvasoid;
            this.Nev = other.Nev;
            this.Cim = other.Cim;
            this.Email = other.Email;
            this.Telefonszam = other.Telefonszam;
            this.Szuletesidatum = other.Szuletesidatum;
        }
    }
}
