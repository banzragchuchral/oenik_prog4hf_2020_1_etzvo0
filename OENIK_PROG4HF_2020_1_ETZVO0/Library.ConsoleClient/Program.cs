﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Library.ConsoleClient
{
    public class Olvaso
    {
        public int Olvasoid { get; set; }

        public string Nev { get; set; }

        public DateTime Szuletesidatum { get; set; }

        public string Cim { get; set; }

        public string Email { get; set; }

        public string Telefonszam { get; set; }

        public override string ToString()
        {
            return $"ID={Olvasoid}\tNev={Nev}\tSzul={Szuletesidatum}\tCim={Cim}\tEmail={Email}\tTelefonszam={Telefonszam}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:52553/api/LibraryApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Olvaso>>(json);
                foreach(var x in list)
                {
                    Console.WriteLine(x);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Olvaso.Olvasoid), "11");
                postData.Add(nameof(Olvaso.Nev), "Valaki2");
                postData.Add(nameof(Olvaso.Cim), "Valaki");
                postData.Add(nameof(Olvaso.Email), "2020.05.07");
                postData.Add(nameof(Olvaso.Telefonszam), "Valaki");
                postData.Add(nameof(Olvaso.Szuletesidatum), "Valaki");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int olvasoID = JsonConvert.DeserializeObject<List<Olvaso>>(json).Single(x => x.Nev == "Valaki2").Olvasoid;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Olvaso.Olvasoid), olvasoID.ToString());
                postData.Add(nameof(Olvaso.Nev), "Valaki");
                postData.Add(nameof(Olvaso.Cim), "Valaki");
                postData.Add(nameof(Olvaso.Email), "2020.05.07");
                postData.Add(nameof(Olvaso.Telefonszam), "Valaki");
                postData.Add(nameof(Olvaso.Szuletesidatum), "Valaki");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.PostAsync(url + "del/" + olvasoID, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
