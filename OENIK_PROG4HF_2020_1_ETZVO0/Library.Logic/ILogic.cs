﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Logic
{
    using Library.Data;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface for logic class.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets a list of books.
        /// </summary>
        /// <returns>List of string.</returns>
        List<string> GetKonyv { get; }

        /// <summary>
        /// Gets a list of library.
        /// </summary>
        /// <returns>List of string.</returns>
        List<string> GetKonyvtar { get; }

        /// <summary>
        /// Gets a list of rents.
        /// </summary>
        /// <returns>List of string.</returns>
        List<string> GetKolcsonzes { get; }

        /// <summary>
        /// Gets a list of readers.
        /// </summary>
        /// <returns>List of string.</returns>
        List<string> GetOlvaso { get; }

        IList<Olvaso> GetOlvaso2();

        /// <summary>
        /// Insert book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="cim">title.</param>
        /// <param name="kiadas">date.</param>
        /// <param name="konyvkateg">categ.</param>
        /// <param name="szerzo">author.</param>
        /// <param name="konyvtarid">konyvtarid.</param>
        /// <returns>boo.</returns>
        bool InsertKonyv(int id, string cim, DateTime kiadas, string konyvkateg, string szerzo, int konyvtarid);

        /// <summary>
        /// Insert library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="fax">fax.</param>
        /// <param name="telefonszam">phone.</param>
        /// <returns>bool.</returns>
        bool InsertKonyvtar(int id, string nev, string cim, string email, string fax, string telefonszam);

        /// <summary>
        /// Insert rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="konyvid">bookid.</param>
        /// <param name="olvasoid">readerid.</param>
        /// <param name="datum">date.</param>
        /// <returns>bool.</returns>
        bool InsertKolcsonzes(int id, int konyvid, int olvasoid, DateTime datum);

        /// <summary>
        /// Insert reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="telefonszam">phone.</param>
        /// <param name="szul">szul.</param>
        /// <returns>bool.</returns>
        bool InsertOlvaso(int id, string nev, string cim, string email,  string telefonszam, DateTime szul);
        bool RemoveReader(int id);
        /// <summary>
        /// Check if book exist.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool CheckifExistKonyv(int id);

        /// <summary>
        /// Check if library exist.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool CheckifExistKonyvtar(int id);

        /// <summary>
        /// Check if rent exist.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool CheckifExistKolcsonzes(int id);

        /// <summary>
        /// Check if reader exist.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool CheckifExistOlvaso(int id);

        /// <summary>
        /// Delete book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool DeleteKonyv(int id);

        /// <summary>
        /// Delete library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool DeleteKonyvtar(int id);

        /// <summary>
        /// Delete rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool DeleteKolcsonzes(int id);

        /// <summary>
        /// Delete reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool DeleteOlvaso(int id);

        /// <summary>
        /// Insert book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="cim">title.</param>
        /// <param name="kiadas">date.</param>
        /// <param name="konyvkateg">categ.</param>
        /// <param name="szerzo">author.</param>
        /// <param name="konyvtarid">konyvtarid.</param>
        /// <returns>boo.</returns>
        bool UpdateKonyv(int id, string cim, DateTime kiadas, string konyvkateg, string szerzo, int konyvtarid);

        /// <summary>
        /// Insert library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="fax">fax.</param>
        /// <param name="telefonszam">phone.</param>
        /// <returns>bool.</returns>
        bool UpdateKonyvtar(int id, string nev, string cim, string email, string fax, string telefonszam);

        /// <summary>
        /// Insert rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="konyvid">bookid.</param>
        /// <param name="olvasoid">readerid.</param>
        /// <param name="datum">date.</param>
        /// <returns>bool.</returns>
        bool UpdateKolcsonzes(int id, int konyvid, int olvasoid, DateTime datum);

        /// <summary>
        /// Insert reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="telefonszam">phone.</param>
        /// <param name="szul">szul.</param>
        /// <returns>bool.</returns>
        bool UpdateOlvaso(int id, string nev, string cim, string email, string telefonszam, DateTime szul);

        Olvaso GetOlvaso2(int id);
    }
}
