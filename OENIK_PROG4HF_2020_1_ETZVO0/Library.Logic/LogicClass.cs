﻿// <copyright file="LogicClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Library.Repository;
    using Library.Data;
    using System.Linq;

    /// <summary>
    /// Class for logic methods.
    /// </summary>
    public class LogicClass : ILogic
    {
        private RepositoryHelper repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicClass"/> class.
        /// This constructor create a new repository.
        /// </summary>
        public LogicClass()
        {
            this.repository = new RepositoryHelper();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicClass"/> class.
        /// </summary>
        /// <param name="repositoryHelper">repo.</param>
        public LogicClass(RepositoryHelper repositoryHelper)
        {
            this.repository = repositoryHelper;
        }

        /// <summary>
        /// Gets the RepositoryHelper instance.
        /// </summary>
        public virtual RepositoryHelper Helper
        {
            get
            {
                return this.repository;
            }
        }

        /// <summary>
        /// Gets books data.
        /// </summary>
        /// <returns>List of string.</returns>
        public List<string> GetKonyv
        {
            get
            {
                return this.repository.KonyvRepository.GetBooks;
            }
        }

        /// <summary>
        /// Gets books data.
        /// </summary>
        /// <returns>List of string.</returns>
        public List<string> GetKonyvtar
        {
            get
            {
                return this.repository.KonyvtarRepository.GetLibraries;
            }
        }

        /// <summary>
        /// Gets books data.
        /// </summary>
        /// <returns>List of string.</returns>
        public List<string> GetKolcsonzes
        {
            get
            {
                return this.repository.KolcsonzesRepository.GetRents;
            }
        }

        /// <summary>
        /// Gets books data.
        /// </summary>
        /// <returns>List of string.</returns>
        public List<string> GetOlvaso
        {
            get
            {
                return this.repository.OlvasoRepository.GetReaders;
            }
        }

        /// <summary>
        /// Checks if a rent exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool CheckifExistKolcsonzes(int id)
        {
            if (id < 0)
            {
                return false;
            }
            else
            {
                return this.repository.KolcsonzesRepository.CheckRentExist(id);
            }
        }

        /// <summary>
        /// Checks if a book exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>book.</returns>
        public bool CheckifExistKonyv(int id)
        {
            return this.repository.KonyvRepository.CheckBookExist(id);
        }

        /// <summary>
        /// Checks if a library exist with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool CheckifExistKonyvtar(int id)
        {
            return this.repository.KonyvtarRepository.CheckLibraryExist(id);
        }

        /// <summary>
        /// Checks if a reader exits with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool CheckifExistOlvaso(int id)
        {
            return this.repository.OlvasoRepository.CheckReaderExist(id);
        }

        /// <summary>
        /// Delete a rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool DeleteKolcsonzes(int id)
        {
            try
            {
                return this.repository.KolcsonzesRepository.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt delete rent id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete a book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool DeleteKonyv(int id)
        {
            try
            {
                return this.repository.KonyvRepository.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt delete book id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete a library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool DeleteKonyvtar(int id)
        {
            try
            {
                return this.repository.KonyvtarRepository.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt delete library id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete a reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public bool DeleteOlvaso(int id)
        {
            try
            {
                return this.repository.OlvasoRepository.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt delete reader id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Insert rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="konyvid">bookid.</param>
        /// <param name="olvasoid">readerid.</param>
        /// <param name="datum">date.</param>
        /// <returns>bool.</returns>
        public bool InsertKolcsonzes(int id, int konyvid, int olvasoid, DateTime datum)
        {
            try
            {
                return this.repository.KolcsonzesRepository.Insert(id, konyvid, olvasoid, datum);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt insert rent id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Insert book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="cim">address.</param>
        /// <param name="kiadas">date.</param>
        /// <param name="konyvkateg">categ.</param>
        /// <param name="szerzo">author.</param>
        /// <param name="konyvtarid">konyvtarid.</param>
        /// <returns>bool.</returns>
        public virtual bool InsertKonyv(int id, string cim, DateTime kiadas, string konyvkateg, string szerzo, int konyvtarid)
        {
            try
            {
                return this.repository.KonyvRepository.Insert(id, cim, kiadas, konyvkateg, szerzo, konyvtarid);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt insert book id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Insert library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="fax">fax.</param>
        /// <param name="telefonszam">phone.</param>
        /// <returns>bool.</returns>
        public bool InsertKonyvtar(int id, string nev, string cim, string email, string fax, string telefonszam)
        {
            try
            {
                return this.repository.KonyvtarRepository.Insert(id, nev, cim, email, fax, telefonszam);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt update library id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Insert reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="telefonszam">phone.</param>
        /// <param name="szul">date.</param>
        /// <returns>bool.</returns>
        public virtual bool InsertOlvaso(int id, string nev, string cim, string email, string telefonszam, DateTime szul)
        {
            try
            {
                return this.repository.OlvasoRepository.Insert(id, nev, cim, email, telefonszam, szul);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt insert id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Update rent.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="konyvid">bookid.</param>
        /// <param name="olvasoid">readerid.</param>
        /// <param name="datum">date.</param>
        /// <returns>bool.</returns>
        public bool UpdateKolcsonzes(int id, int konyvid, int olvasoid, DateTime datum)
        {
            try
            {
                return this.repository.KolcsonzesRepository.Update(id, konyvid, olvasoid, datum);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt update rent id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Insert book.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="cim">address.</param>
        /// <param name="kiadas">date.</param>
        /// <param name="konyvkateg">categ.</param>
        /// <param name="szerzo">author.</param>
        /// <param name="konyvtarid">konyvtarid.</param>
        /// <returns>bool.</returns>
        public virtual bool UpdateKonyv(int id, string cim, DateTime kiadas, string konyvkateg, string szerzo, int konyvtarid)
        {
            try
            {
                return this.repository.KonyvRepository.Update(id, cim, kiadas, konyvkateg, szerzo, konyvtarid);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt update book id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Update library.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="fax">fax.</param>
        /// <param name="telefonszam">phone.</param>
        /// <returns>bool.</returns>
        public bool UpdateKonyvtar(int id, string nev, string cim, string email, string fax, string telefonszam)
        {
            try
            {
                return this.repository.KonyvtarRepository.Update(id, nev, cim, email, fax, telefonszam);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt update library id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Update reader.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="cim">address.</param>
        /// <param name="email">email.</param>
        /// <param name="telefonszam">phone.</param>
        /// <param name="szul">date.</param>
        /// <returns>bool.</returns>
        public virtual bool UpdateOlvaso(int id, string nev, string cim, string email, string telefonszam, DateTime szul)
        {
            try
            {
                return this.repository.OlvasoRepository.Update(id, nev, cim,  szul, email, telefonszam);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldnt update id: " + id + ", Exception message: " + e.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns the number of phone number according to the received number.
        /// </summary>
        /// <param name="szam">szam.</param>
        /// <returns>object.</returns>
        public IEnumerable<object> HanyMobil(string szam)
        {
            return this.repository.OlvasoRepository.Hanymobilszam(szam);
        }

        /// <summary>
        /// Return how many rents are in the dabase.
        /// </summary>
        /// <returns>int.</returns>
        public int Hanykolcsonzes()
        {
            return this.repository.KolcsonzesRepository.HanyKolcsonzes();
        }

        /// <summary>
        /// Returns how many book are there with this id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>int.</returns>
        public int HanyKonyv(int id)
        {
            return this.repository.KonyvtarRepository.Hanykonyv(id);
        }

        /// <summary>
        /// Returns a book of obects.
        /// </summary>
        /// <param name="ev">ev.</param>
        /// <returns>IEnumarble.</returns>
        public IEnumerable<object> Evalatt(int ev)
        {
            return this.repository.KonyvRepository.Evalatt(ev);
        }

        public Olvaso GetOlvaso2(int id)
        {
            return this.repository.OlvasoRepository.GetOne(id);
        }

        public IList<Olvaso> GetOlvaso2()
        {
            return this.repository.OlvasoRepository.GetAll2().ToList();
        }

        public bool RemoveReader(int id)
        {
            return this.repository.OlvasoRepository.Remove(id);
        }
    }
}
