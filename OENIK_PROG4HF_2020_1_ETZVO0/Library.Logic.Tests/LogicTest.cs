﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Library.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Library.Data;
    using Library.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Handles the tests for the logic class.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        /// <summary>
        /// Book insert check.
        /// </summary>
        [Test]
        public void BookInsertCheck()
        {
            LogicClass m = Mock.Of<LogicClass>();
            Mock<LogicClass> m2 = Mock.Get(m);
            List<Konyv> lista = new List<Konyv>()
            {
                new Konyv() { Konyvid = 100, Cim = "Valami1", Konyvkateg = "kateg1", Szerzo = "valaki1", Konyvtarid = 4, Kiadas = DateTime.Today },
            };
            m2.Setup(x => x.Helper.KonyvRepository.GetAll).Returns(lista.AsQueryable());
            m2.Object.InsertKonyv(105, "Valami2", DateTime.Today, "kateg2", "Valaki2", 5);
            m2.Verify(mock => mock.InsertKonyv(105, "Valami2", DateTime.Today, "kateg2", "Valaki2", 5), Times.Once());
        }

        /// <summary>
        /// Reader insert check.
        /// </summary>
        [Test]
        public void ReaderInsertCheck()
        {
            LogicClass m = Mock.Of<LogicClass>();
            Mock<LogicClass> m2 = Mock.Get(m);
            List<Olvaso> lista = new List<Olvaso>()
            {
                new Olvaso() { Olvasoid = 100, Cim = "asd utca",  Nev = "valaki1", Email = "asd@asd.com",  Szuletesidatum = DateTime.Today, Telefonszam = "0123123" },
            };
            m2.Setup(x => x.Helper.OlvasoRepository.GetAll).Returns(lista.AsQueryable());
            m2.Object.InsertOlvaso(105, "valaki2", "asd", "asd2@asd.com", "01231243", DateTime.Today);
            m2.Verify(mock => mock.InsertOlvaso(105, "valaki2", "asd", "asd2@asd.com", "01231243", DateTime.Today), Times.Once());
        }

        /// <summary>
        /// Library insert check.
        /// </summary>
        [Test]
        public void LibraryInsertCheck()
        {
            LogicClass m = Mock.Of<LogicClass>();
            Mock<LogicClass> m2 = Mock.Get(m);
            List<Konyv> lista = new List<Konyv>()
            {
                new Konyv() { Konyvid = 100, Cim = "Valami1", Konyvkateg = "kateg1", Szerzo = "valaki1", Konyvtarid = 4, Kiadas = DateTime.Today },
            };
            m2.Setup(x => x.Helper.KonyvRepository.GetAll).Returns(lista.AsQueryable());
            m2.Object.InsertKonyv(105, "Valami2", DateTime.Today, "kateg2", "Valaki2", 5);
            m2.Verify(mock => mock.InsertKonyv(105, "Valami2", DateTime.Today, "kateg2", "Valaki2", 5), Times.Once());
        }

        /// <summary>
        /// Rent insert check.
        /// </summary>
        [Test]
        public void RentInsertCheck()
        {
            LogicClass m = Mock.Of<LogicClass>();
            Mock<LogicClass> m2 = Mock.Get(m);
            List<Konyv> lista = new List<Konyv>()
            {
                new Konyv() { Konyvid = 100, Cim = "Valami1", Konyvkateg = "kateg1", Szerzo = "valaki1", Konyvtarid = 4, Kiadas = DateTime.Today },
            };
            m2.Setup(x => x.Helper.KonyvRepository.GetAll).Returns(lista.AsQueryable());
            m2.Object.InsertKonyv(105, "Valami2", DateTime.Today, "kateg2", "Valaki2", 5);
            m2.Verify(mock => mock.InsertKonyv(105, "Valami2", DateTime.Today, "kateg2", "Valaki2", 5), Times.Once());
        }

        /// <summary>
        /// Reader update check.
        /// </summary>
        [Test]
        public void ReaderUpdateCheck()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.OlvasoRepository.Update(1, "Valaki", "Valahol", DateTime.Today, "asd", "telefon")).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.UpdateOlvaso(1, "Valaki", "Valahol", "asd", "telefon", DateTime.Today);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.OlvasoRepository.Update(1, "Valaki", "Valahol", DateTime.Today, "asd", "telefon"), Times.Once);
        }

        /// <summary>
        /// Book update check.
        /// </summary>
        [Test]
        public void BookUpdateCheck()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvRepository.Update(1, "valahol", DateTime.Today, "kateg", "szerzo", 1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.UpdateKonyv(1, "valahol", DateTime.Today, "kateg", "szerzo", 1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.KonyvRepository.Update(1, "valahol", DateTime.Today, "kateg", "szerzo", 1), Times.Once);
        }

        /// <summary>
        /// Library update check.
        /// </summary>
        [Test]
        public void LibraryUpdateCheck()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvtarRepository.Update(1, "valahol", "valami", "kateg", "szerzo", "telefonszam")).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.UpdateKonyvtar(1, "valahol", "valami", "kateg", "szerzo", "telefonszam");
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.KonyvtarRepository.Update(1, "valahol", "valami", "kateg", "szerzo", "telefonszam"), Times.Once);
        }

        /// <summary>
        /// Delete reader.
        /// </summary>
        [Test]
        public void DeleteReaderCheck()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.OlvasoRepository.Delete(1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.DeleteOlvaso(1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.OlvasoRepository.Delete(1), Times.Once);
        }

        /// <summary>
        /// Delete rents.
        /// </summary>
        [Test]
        public void DeleteRentCheck()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KolcsonzesRepository.Delete(1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.DeleteKolcsonzes(1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.KolcsonzesRepository.Delete(1), Times.Once);
        }

        /// <summary>
        /// Delete rents.
        /// </summary>
        [Test]
        public void DeleteLibraryCheck()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvtarRepository.Delete(1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.DeleteKonyvtar(1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.KonyvtarRepository.Delete(1), Times.Once);
        }

        /// <summary>
        /// Rent exist.
        /// </summary>
        [Test]
        public void RentExist()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KolcsonzesRepository.CheckRentExist(1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.CheckifExistKolcsonzes(1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.KolcsonzesRepository.CheckRentExist(1), Times.Once);
        }

        /// <summary>
        /// Reader exist.
        /// </summary>
        [Test]
        public void ReaderExist()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.OlvasoRepository.CheckReaderExist(1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.CheckifExistOlvaso(1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.OlvasoRepository.CheckReaderExist(1), Times.Once);
        }

        /// <summary>
        /// Library exist.
        /// </summary>
        [Test]
        public void LibraryExist()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvtarRepository.CheckLibraryExist(1)).Returns(true);
            LogicClass logic = new LogicClass(mockRepo.Object);
            bool result = logic.CheckifExistKonyvtar(1);
            Assert.That(result, Is.EqualTo(true));
            mockRepo.Verify(repo => repo.KonyvtarRepository.CheckLibraryExist(1), Times.Once);
        }

        /// <summary>
        /// Checks if GetAll property is working.
        /// </summary>
        [Test]
        public void LibraryAll()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvtarRepository.GetAll);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.GetKonyvtar;
            Assert.That(result, Is.EqualTo(mockRepo.Object.KonyvtarRepository.GetAll));
            mockRepo.Verify(repo => repo.KonyvtarRepository.GetAll, Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if GetAll property is working.
        /// </summary>
        [Test]
        public void RentAll()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KolcsonzesRepository.GetAll);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.GetKolcsonzes;
            Assert.That(result, Is.EqualTo(mockRepo.Object.KolcsonzesRepository.GetAll));
            mockRepo.Verify(repo => repo.KolcsonzesRepository.GetAll, Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if GetAll property is working.
        /// </summary>
        [Test]
        public void BookAll()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvRepository.GetAll);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.GetKonyv;
            Assert.That(result, Is.EqualTo(mockRepo.Object.KonyvRepository.GetAll));
            mockRepo.Verify(repo => repo.KonyvRepository.GetAll, Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if HanyKolcsonzes function is working.
        /// </summary>
        [Test]
        public void HanyKolcsonzesTest()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KolcsonzesRepository.HanyKolcsonzes()).Returns(3);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.Hanykolcsonzes();
            Assert.That(result, Is.EqualTo(3));
            mockRepo.Verify(repo => repo.KolcsonzesRepository.HanyKolcsonzes(), Times.Once);
        }

        /// <summary>
        /// Checks if EvAlatt function is working.
        /// </summary>
        [Test]
        public void EvAlattTest()
        {
            List<object> list = new List<object>();
            list.Add("valami");
            IEnumerable<object> eredmeny = list;

            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvRepository.Evalatt(2000)).Returns(eredmeny);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.Evalatt(2000);
            Assert.That(result, Is.EqualTo(eredmeny));
            mockRepo.Verify(repo => repo.KonyvRepository.Evalatt(2000), Times.Once);
        }

        /// <summary>
        /// Checks if HanyMobilTest function is working.
        /// </summary>
        [Test]
        public void HanyMobilTest()
        {
            List<object> list = new List<object>();
            list.Add("valami");
            IEnumerable<object> eredmeny = list;

            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.OlvasoRepository.Hanymobilszam("30")).Returns(eredmeny);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.HanyMobil("30");
            Assert.That(result, Is.EqualTo(eredmeny));
            mockRepo.Verify(repo => repo.OlvasoRepository.Hanymobilszam("30"), Times.Once);
        }

        /// <summary>
        /// Checks if HanyKonyvTest function is working.
        /// </summary>
        [Test]
        public void HanyKonyvTest()
        {
            Mock<RepositoryHelper> mockRepo = new Mock<RepositoryHelper>();
            mockRepo.Setup(repo => repo.KonyvtarRepository.Hanykonyv(1)).Returns(1);
            LogicClass logic = new LogicClass(mockRepo.Object);
            var result = logic.HanyKonyv(1);
            Assert.That(result, Is.EqualTo(1));
            mockRepo.Verify(repo => repo.KonyvtarRepository.Hanykonyv(1), Times.Once);
        }
    }
}
